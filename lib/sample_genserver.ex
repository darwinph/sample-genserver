defmodule SampleGenserver do
  @moduledoc """
  Documentation for SampleGenserver.
  """

  @doc """
  Hello world.

  ## Examples

      iex> SampleGenserver.hello()
      :world

  """
  def hello do
    :world
  end
end
