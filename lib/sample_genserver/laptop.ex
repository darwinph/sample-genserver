defmodule Laptop do
  @moduledoc """
  Idiot-friendly doc

  In `SampleGenserver.Application` we include the `Laptop` as a child process so it starts when the application starts. We also use "darwin" as argument that sets the default value/state that invoked `Laptop.init/1` function.

  Alternatively, if we don't want to assign an argument, we can just set it directly in the `init/1` function like this:

  ```
  def init(_args) do
    password = "darwin"
    {:ok, password}
  end
  ```

  In application.ex
  Instead of putting it inside of a tuple:
  ```
  children = [
      {Laptop, "darwin"}
    ]
  ```

  We could just call the `Laptop` module:

  ```
  children = [
      Laptop
    ]
  ```

  WTF are these?
  `init/1`, `start_link/1`, `handle_call/3`, handle_cast/2 are GenServer implementation
  So for example in `Laptop.owner_name/0` function

  `GenServer.call(__MODULE__, :get_owner_name)`

  The `:get_owner_name` just invokes `handle_call(:get_owner_name, _from, current_state)` function

  It just pattern matched the 2nd arg of GenServer.call/2 and the 1st arg of handle_call/3

  USAGE:

  iex> Laptop.retrieve_password
  "darwin"

  iex> Laptop.unlock("darwin")
  {:ok, "Laptop unlocked!"}

  iex> Laptop.owner_name
  {:ok, "Darwin Manalo"}

  iex> Laptop.manufacturer
  "Apple Inc."
  """

  use GenServer

  @impl true
  def init(args) when is_binary(args) do
    {:ok, args}
  end

  def start_link(initial_password) do
    GenServer.start_link(__MODULE__, initial_password, name: __MODULE__)
  end

  # External API

  def unlock(password) do
    GenServer.call(__MODULE__, {:unlock, password})
  end

  def owner_name do
    GenServer.call(__MODULE__, :get_owner_name)
  end

  def manufacturer do
    GenServer.call(__MODULE__, :get_manufacturer)
  end

  def laptop_type do
    GenServer.call(__MODULE__, :get_type)
  end

  def retrieve_password do
    GenServer.call(__MODULE__, :get_password)
  end

  def laptop_specs do
    GenServer.call(__MODULE__, :get_specs)
  end

  def change_password(old_password, new_password) do
    GenServer.cast(__MODULE__, {:change_password, old_password, new_password})
  end

  # GenServer Implementation

  @impl true
  def handle_call(:get_password, _from, current_password) do
    {:reply, current_password, current_password}
  end

  @impl true
  def handle_call(:get_manufacturer, _from, current_state) do
    {:reply, "Apple Inc.", current_state}
  end

  @impl true
  def handle_call(:get_type, _from, current_state) do
    {:reply, "MacBook Pro", current_state}
  end

  @impl true
  def handle_call(:get_owner_name, _from, current_state) do
    {:reply, {:ok, "Darwin Manalo"}, current_state}
  end

  @impl true
  def handle_call(:get_specs, _from, current_state) do
    {:reply, {:ok, ["2.9 GHz Intel Core i5"], 8192, :intel_iris_graphics}, current_state}
  end

  @impl true
  def handle_call(:name_check, _from, current_state) do
    {:reply, "Congrats! Your process was successfully named.", current_state}
  end

  @impl true
  def handle_call({:unlock, password}, _from, current_password) do
    case password do
      password when password === current_password ->
        {:reply, {:ok, "Laptop unlocked!"}, current_password}

      _ ->
        {:reply, {:error, "Incorrect password!"}, current_password}
    end
  end

  @impl true
  def handle_cast({:change_password, old_password, new_password}, current_password) do
    case old_password do
      old_password when old_password == current_password ->
        {:noreply, new_password}

      _ ->
        {:noreply, current_password}
    end
  end
end
